#include <iostream>
#include <fstream>

using namespace std;
///Opens and read the file.
///@param *size is the size of the integers array.
///@returns array of integers.
int *fileOperations(int *size);
///Adds integers
///@returns summation value.
int sumOfIntegers(int arr[], int size);
///Finds average of integers.
///
/// Uses sumOfIntegers() s return value and calculates average.
///@returns average value.
float averageOfIntegers(int sum ,int size);
///Finds product of integers.
///@returns product value.
int productOfIntegers(int arr[],int size);
///Finds smallest member of integers.
///@returns smallest integer.
int smallestInteger(int arr[],int size);
///Prints results.
///@param sum is the sum of integers.
///@param av is the average of integers.
///@param pr is the product of integers.
///@param sm is the smallest member of integers.
void printResults(int sum, float av, int pr, int sm);

int main()
{
    int arr_size, sum, product, smallest;
    float average;
    int *arr = fileOperations(&arr_size);
    sum = sumOfIntegers(arr, arr_size);
    average = averageOfIntegers(sum,arr_size);
    product = productOfIntegers(arr, arr_size);
    smallest = smallestInteger(arr, arr_size);
    printResults(sum,average,product,smallest);
    return 0;
}

int *fileOperations(int *size)
{
    ifstream inputFile; string fileName;

    cout << "Enter file name: ";
    cin >> fileName;

    inputFile.open(fileName, ios::in);

    if (!inputFile.is_open())
    {
        cout << "File cannot opened!" << endl;
        exit(1);
    }
    inputFile >> *size;
    int* inputArray = new int[*size];

    for (int i = 0; !inputFile.eof(); i++)
    {
        inputFile >> inputArray[i];
    }
    inputFile.close();
    return inputArray;
}

int sumOfIntegers(int arr[], int size)
{
    int sum=0;
    for (int i = 0; i < size; i++)
    {
        sum += arr[i];
    }
    return sum;
}

float averageOfIntegers(int sum ,int size)
{
    return (float) sum/size;
}

int productOfIntegers(int arr[],int size)
{
     int product=1;
    for (int i = 0; i < size; i++)
    {
        product = product * arr[i];
    }
    return product;
}

int smallestInteger(int arr[],int size)
{
    int smallest = arr[0];
    for (int i = 1; i < size; i++)
    {
        if (arr[i] < smallest)
        {
            smallest = arr[i];
        }

    }
    return smallest;
}

void printResults(int sum, float av, int pr, int sm)
{
    cout << "Sum of numbers: " << sum << endl;
    cout << "Product of numbers: " << pr << endl;
    cout << "Average of numbers: " << av << endl;
    cout << "Smallest number: " << sm << endl;
}
